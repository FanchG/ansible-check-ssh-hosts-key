Check Ssh Known Hosts Key
=========================

Some playbook to help check & manage ~/.ssh/known_hosts

It should be used to detect unattended change on network based on ssh key stored in your known_hosts

Check Host's Keys
-----------------

```bash
ansible-playbook -v check_hosts_key.yml
```
*Maybe you should cron it...*

Hostslist
---------

The file [hosts](hosts) contains the liste of host to check (one host per line)

*You should edit it to fit your need...*

Email
-----

To receive email on change you should edit [ansible.cfg](ansible.cfg) to uncomment the following line:

```ini
#[callback_mail]
#to =
#sender =
#smtphost =

```
*And fill them with your email and smtp server information...*

First run
---------

*it's not mandatory, but to avoid duplicate it's maybe a good idea to remove ~/.ssh/known_hosts before adding key*

```bash
ansible-playbook -v rm_known_hosts.yml
```

**This is mandatory:** Before your first check and after any update to the [hosts](hosts) you should run:

```bash
ansible-playbook -v add_key_to_known_hosts.yml
```

/etc/ssh/ssh_config
-------------------

Those playbooks should also work if you have this setting in your /etc/ssh/ssh_config :

```
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```

License
-------

[License](LICENSE)
